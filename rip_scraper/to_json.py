"""
USAGE: rip-json
"""
import docopt
import csv
import json
import datetime
import sys

QUAL_NAMES = {
    "AS": "Autonomous Systems",
    "SN": "Sensor Network",
    "WD": "Workforce Development",
    "IM": "Inspection & Maintenance",
    "RR": "Retrofit Resilience",   
    "TT": "TEST TEST",
}

def main():
    args = docopt.docopt(__doc__)
    
    data = []
    for row in csv.reader(sys.stdin):
        data.append({
            "title": row[0],
            "pi": row[1],
            "organization": row[2],
            "innerHTML": row[3],
            "status": row[4],
            "type": row[5],
            "typeId": row[6],
            "typeName": QUAL_NAMES.get(row[5], None)
})
    
    document = {
            "version": 1,
            "updated": datetime.datetime.now().strftime("%m/%d/%Y"),     
            "data": data,
    }
    
    json.dump(document, sys.stdout)


if __name__ == "__main__":
    main()
