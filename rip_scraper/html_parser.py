# This script extracts the fields from the html form scraped from rip.trb.org/edit
from bs4 import BeautifulSoup
import docopt
import sys
import collections
import csv
import parse

def main():
    """
    Read in html from stdin, and output CSV to stdout. 
    The format of the output is:
    
    Title, PI, PI Organization, innerHTML, Status Type, Type_ID

    FIELDS:
        Title - project title
        PI - project investigator
        PI Organization - The institution the PI works for
        innerHTML - Text of the rip-trb.org edit page
        Status - status of the project (Active or Complete)
        Type (PRIMARY KEY) - One of:
            WD (Workforce Development)
            AS (Autonomous System)
            IM (Infrastructure Management)
            RR ()

            NULL
        Type_ID (PRIMARY KEY) - An integer, or NULL
    """
    

    soup = BeautifulSoup(sys.stdin.read(), "html.parser")
   

    title = ""
    pi_organization = ""
    pi = ""
    status = ""
    Type = None
    Type_ID = None
 
    innerHTML = elt = soup.find(id="trid-record-main")
    
    title = soup.find(id="record-title").getText().strip()
    result =  parse.parse("{}({}-{})", title)
    if result:
        title, Type, Type_ID = result
     
     
    for elt in elt.find_all("li"):
        text = elt.getText()
        
        if "Status: " in text:
            status = text.partition(":")[2].strip()
            continue  
        if "Principal Investigators: " in text:   
            pi = elt.find("span", class_="entry-name").getText()
            continue
        if "Performing Organizations:" in text:
            pi_organization = text.partition(":")[2].strip().split("\n")[0]
            continue
    # Wrap in csv so it is easier to join
    writer = csv.writer(sys.stdout)
    writer.writerow([title, pi, pi_organization, innerHTML, status, Type, Type_ID])

if __name__ == "__main__":
    main()
