
import sys


def main():    
    print(" ".join(line.strip() for line in sys.stdin.read().split("\n") if line.startswith("https://")))


if __name__ == "__main__":
    main()
